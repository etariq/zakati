<?php include('header.php'); ?>
				
                <div class="wrapper" id="google">
                    <div class="banner banner--mid-page">
                        <div class="js-search-close">
                <div class="js-slideshow js-slideshow-autoplay">
                    <div class="js-gallery">
                        <div class="wrapper js-scroll-top">
                            <div class="banner js-animate-on-load">
                                <div class="video-container js-video-container">
                                    <div id="youtube-player"></div>
                                    <div class="video-close js-video-close js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-1:close-video">
                                        <span class="icon icon--video-close"></span>
                                    </div>
                                </div>
                                <div class="banner-image--container banner-image--carousel">
                                    <div class="banner-image">
                                        <div class="slideshow">
                                            <div class="js-gallery-slide slideshow--item absolute-img">
                                                <a class="banner-image-link js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-2:version-learn-more" href="resaux-zakati.php">
                                                    <div class="banner-absolute-image banner--version">
                                                        <div class="version">
                                                            <h2 class="heading-2">
															

                          Le premier service mondial en ligne de collecte et redistribution de la Zakat 
                        <div class="span-slid"><span class="text-button">
                          Lire plus
                        </span></div>
						</h2>
                         
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="js-gallery-slide slideshow--item absolute-img--nexus">
                                                <a class="banner-image-link js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-3:offsite:nexus-learn-more" href="campagnes.php" target="_blank">
                                                    <div class="banner-absolute-image banner--nexus">
                                                        <div class="nexus">
                                                            <h2 class="heading-2">
                         
                        </h2>
                                                            <p>
                                                              
                                                            </p>
                                                            <span class="text-button">
                       
                        </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="js-gallery-slide slideshow--item absolute-img">
                                                <div class="banner-absolute-image banner--androidify">
                                                    <a class="slide1-play-link js-video-play js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-1:play-video" data-href="OhfAIIYEyIU" href="https://www.youtube.com/watch?v=OhfAIIYEyIU"></a>
                                                    <div class="betohether1">
                                                        
                                                    </div>
                                                    <div class="betohether2">
                                                        Gratuité, neutralité, liberté, entraide, égalité, traçabilité, transparence et modération des données.
                                                    </div>
                                                    <div class="betohether3">
                                                        <a class="js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-1:offsite:androidify" href="https://www.youtube.com/watch?v=OhfAIIYEyIU" target="_blank">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="floating-action--large header-fab js-fab js-scroll js-analytics" data-g-action="click" data-g-event="home:hero-fab" data-g-label="scroll-to-first" data-offset="180" data-target="js-scroll-first">
                                <span class="icon icon--blue-grey-arrow-down-large"></span>
                            </div>
                        </div>
                        <div class="slideshow-controller__container">
                            <span class="slides-pagination-item slideshow-controller__prev js-slideshow-tab" data-tab-id="-1"><span class="icon icon--green-arrow-left"></span></span>
                            <div class="slides-pagination-item">
                                <div class="slides-pagination-link js-gallery-trigger js-video-close js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-pagination-2"></div>
                            </div>
                            <div class="slides-pagination-item">
                                <div class="slides-pagination-link js-gallery-trigger js-video-close js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-pagination-3"></div>
                            </div>
                            <div class="slides-pagination-item">
                                <div class="slides-pagination-link js-gallery-trigger js-video-close js-analytics" data-g-action="click" data-g-event="home:hero-carousel" data-g-label="slide-pagination-4"></div>
                            </div>
                            <span class="slides-pagination-item slideshow-controller__next js-slideshow-tab" data-tab-id="1"><span class="icon icon--green-arrow-right"></span></span>
                        </div>
                    </div>
                </div>
                <div class="wrapper wrapper--above-devices js-scroll-first">
                    <div class="section-header">
                        <h2 class="heading-2 text--center mobile-one-whole">
        Powering screens of all sizes
      </h2>
                        <p class="one-half mobile-one-whole text--center">
                            Android is the customizable, easy to use operating system that powers more than a billion devices across the globe &mdash; from phones and tablets to watches, TV, cars and more to come.
                        </p>
                    </div>
                </div>
                <div class="section section--devices section--top-buffer">
                    <div class="device-container">
                        <div class="devices js-devices">
                            <div class="device wear js-device mobile-shade__wrap">
                                <a class="device-img-link js-analytics" data-g-action="click" data-g-event="home:devices" data-g-label="android-wear" href="wear/index.html">
                                    <div class="device-img-wrap">
                                        <img alt="" class="device-img -off" src="static/img/device-slider/wear-off.jpg"> <img alt="" class="device-img -on js-device-hide" src="static/img/device-slider/wear-on.jpg">
                                        <div class="device-img -on js-device-animate">
                                            <img alt="" class="device-left js-device-show" data-src="/static/img/device-slider/wear-black-on.png" src="index.html"> <img alt="" class="device-right js-device-show" data-src="/static/img/device-slider/wear-silver-on.png" src="index.html">
                                        </div>
                                    </div>
                                    <p class="text-button" href="#">
                                        Android Wear
                                    </p>
                                </a>
                            </div>
                            <div class="device phone js-device mobile-shade__wrap">
                                <a class="device-img-link js-analytics" data-g-action="click" data-g-event="home:devices" data-g-label="phones" href="https://www.android.com/phones/">
                                    <div class="device-img-wrap">
                                        <img alt="" class="device-img -off" src="static/img/device-slider/nexus6-off.jpg"> <img alt="" class="device-img -on" src="static/img/device-slider/nexus6-on.jpg">
                                    </div>
                                    <p class="text-button" href="#">
                                        Phones
                                    </p>
                                </a>
                            </div>
                            <div class="device tablet js-device mobile-shade__wrap">
                                <a class="device-img-link js-analytics" data-g-action="click" data-g-event="home:devices" data-g-label="tablets" href="tablets/index.html">
                                    <div class="device-img-wrap">
                                        <img alt="" class="device-img -off" src="static/img/device-slider/nexus9-off.jpg"> <img alt="" class="device-img -on" src="static/img/device-slider/nexus9-on.jpg">
                                    </div>
                                    <p class="text-button" href="#">
                                        Tablets
                                    </p>
                                </a>
                            </div>
                            <div class="device tv js-device mobile-shade__wrap">
                                <a class="device-img-link js-analytics" data-g-action="click" data-g-event="home:devices" data-g-label="android-tv" href="tv/index.html">
                                    <div class="device-img-wrap">
                                        <img alt="" class="device-img -off" src="static/img/device-slider/tv-off.jpg"> <img alt="" class="device-img -on" src="static/img/device-slider/tv-on.jpg">
                                    </div>
                                    <p class="text-button" href="#">
                                        Android TV
                                    </p>
                                </a>
                            </div>
                            <div class="device auto mobile-shade__wrap js-device">
                                <a class="device-img-link js-analytics" data-g-action="click" data-g-event="home:devices" data-g-label="android-auto" href="auto/index.php">
                                    <div class="device-img-wrap">
                                        <img alt="" class="device-img -off" src="static/img/device-slider/auto-off.jpg"> <img alt="" class="device-img -on" src="static/img/device-slider/auto-on.jpg"> <img alt="" class="device-img -mobile" src="static/img/device-slider/auto-on_mobile.jpg">
                                    </div>
                                    <p class="text-button device__link-mobile" href="#">
                                        Android Auto
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                        <div class="banner-block banner-block--dark-blue block one-whole mobile-is-visible">
                            <div class="container">
                                <h2 class="heading-2 text--bg-is-dark">
            The best of Google built in
          </h2>
                                <p class="heading-3 text--bg-is-dark optimal-readability">
                                    Android works perfectly with your favorite apps like Google Maps, Calendar, Gmail, and YouTube.
                                </p><a class="text-button js-analytics" data-g-action="click" data-g-event="home:lifestyle-photo" data-g-label="see-whats-new" href="#" target="_blank">
            See what's new <span class=
              "icon icon--right icon--teal-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section--home-customized section--no-bottom-buffer" id="customize">
                    <div class="section-header">
                        <h2 class="heading-2">
        Customized by you, for you
      </h2>
                        <p class="one-half mobile-one-whole">
                            Put the stuff you care about right on your homescreen: the latest news, weather, or a stream of your recent photos.
                        </p>
                        <p class="one-half mobile-one-whole">
                            <a class="js-analytics" data-g-action="click" data-g-event="home:product-grid" data-g-label="customize-your-phone" href="https://www.android.com/phones/#tips">
        Customize your phone
      </a>
                        </p>
                    </div><img alt="" class="home--devices-image" src="static/img/home/devices.jpg">
                </div>
                <div class="section section--promo-cards section--flat" id="promocards">
                    <div class="section-header card-grid__header">
                        <h2 class="heading-2">
        Nous Services
      </h2>
                    </div>
                    <div class="js-slideshow">
                        <div class="js-gallery">
                            <div class="slideshow js-match-all-heights">
                                <div class="js-gallery-slide slideshow--item">
                                    <div class="container container__mobile--no-gutter">
                                        <div class="row">
                                            <div class="column one-third mobile-one-whole gutter-auto">
                                                <figure class="card">
                                                    <div class="card-grid__container js-match-height">
                                                        <div class="card-image">
                                                            <img alt="" class="top-radius" src="static/img/home/android-for-work.jpg" style="height: 228px;" data-match-heights-ignore="true">
                                                        </div>
                                                        <figcaption class="card-footer">
                                                            <div class="block">
                                                                <h3 class="heading-3 card-grid__title allow-orphan">
                                Faire un prêt
                              </h3>
                                                                <p class="card-grid__description">
                                                                    Join us online to hear how you can mobilize your business with Android for Work.
                                                                    <br />November 4, 2015
                                                                </p>
                                                                <a class="text-button text-button--bottom js-analytics" data-g-event="home:content-card" data-g-action="click" data-g-label="Register now" href="https://atmosphere.withgoogle.com/live/android" target="">
                                Register now&nbsp;
                                <span class="icon icon--right icon--green-arrow-right"></span>
                              </a>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="column one-third mobile-one-whole gutter-auto">
                                                <figure class="card">
                                                    <div class="card-grid__container js-match-height">
                                                        <div class="card-image">
                                                            <img alt="" class="top-radius" src="static/img/home/android-pay.png" style="height: 228px;" data-match-heights-ignore="true">
                                                        </div>
                                                        <figcaption class="card-footer">
                                                            <div class="block">
                                                                <h3 class="heading-3 card-grid__title allow-orphan">
                                Faire un don
                              </h3>
                                                                <p class="card-grid__description">
                                                                    Forget fumbling through your wallet and handing over a card. Next time you're at a store, simply tap, pay and you're good to go
                                                                </p>
                                                                <a class="text-button text-button--bottom js-analytics" data-g-event="home:content-card" data-g-action="click" data-g-label="Learn more" href="pay.2" target="">
                                Learn more&nbsp;
                                <span class="icon icon--right icon--green-arrow-right"></span>
                              </a>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="column one-third mobile-one-whole gutter-auto">
                                                <figure class="card">
                                                    <div class="card-grid__container js-match-height">
                                                        <div class="card-image">
                                                            <img alt="" class="top-radius" src="static/img/home/more-from-1.png" style="height: 228px;" data-match-heights-ignore="true">
                                                        </div>
                                                        <figcaption class="card-footer">
                                                            <div class="block">
                                                                <h3 class="heading-3 card-grid__title allow-orphan">
                                Établir un partenariat
                              </h3>
                                                                <p class="card-grid__description">
                                                                    Four tips to make your switch to Android lickety-split
                                                                </p>
                                                                <a class="text-button text-button--bottom js-analytics" data-g-event="home:content-card" data-g-action="click" data-g-label="Make the Switch" href="switch/index.html" target="">
                                Make the Switch&nbsp;
                                <span class="icon icon--right icon--green-arrow-right"></span>
                              </a>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-gallery-slide slideshow--item">
                                    <div class="container container__mobile--no-gutter">
                                        <div class="row">
                                            <div class="column one-third mobile-one-whole gutter-auto">
                                                <figure class="card">
                                                    <div class="card-grid__container js-match-height">
                                                        <div class="card-image">
                                                            <img alt="" class="top-radius" src="static/img/home/more-from-4.png" data-match-heights-ignore="true">
                                                        </div>
                                                        <figcaption class="card-footer">
                                                            <div class="block">
                                                                <h3 class="heading-3 card-grid__title allow-orphan">
                                Create your own Android character
                              </h3>
                                                                <p class="card-grid__description">
                                                                    Turn the little green Android mascot into you, your friends, anyone!
                                                                </p>
                                                                <a class="text-button text-button--bottom js-analytics" data-g-event="home:content-card" data-g-action="click" data-g-label="Androidify.com" href="http://www.androidify.com/" target="_blank">
                                Androidify.com&nbsp;
                                <span class="icon icon--right icon--green-arrow-right"></span>
                              </a>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="column one-third mobile-one-whole gutter-auto">
                                                <figure class="card">
                                                    <div class="card-grid__container js-match-height">
                                                        <div class="card-image">
                                                            <img alt="" class="top-radius" src="static/img/home/more-from-2.png" data-match-heights-ignore="true">
                                                        </div>
                                                        <figcaption class="card-footer">
                                                            <div class="block">
                                                                <h3 class="heading-3 card-grid__title allow-orphan">
                                Get a clean customizable home screen
                              </h3>
                                                                <p class="card-grid__description">
                                                                    A clean, simple, customizable home screen that comes with the power of Google Now: traffic alerts, weather, and much more, just a swipe away
                                                                </p>
                                                                <a class="text-button text-button--bottom js-analytics" data-g-event="home:content-card" data-g-action="click" data-g-label="Download now" href="http://g.co/nowlauncher" target="_blank">
                                Download now&nbsp;
                                <span class="icon icon--right icon--green-arrow-right"></span>
                              </a>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </figure>
                                            </div>
                                            <div class="column one-third mobile-one-whole gutter-auto">
                                                <figure class="card">
                                                    <div class="card-grid__container js-match-height">
                                                        <div class="card-image">
                                                            <img alt="" class="top-radius" src="static/img/home/more-from-3.png" data-match-heights-ignore="true">
                                                        </div>
                                                        <figcaption class="card-footer">
                                                            <div class="block">
                                                                <h3 class="heading-3 card-grid__title allow-orphan">
                                Millions to choose from
                              </h3>
                                                                <p class="card-grid__description">
                                                                    Hail a taxi, find a recipe, run through a temple—Google Play has all the apps and games that let you make your Android device uniquely yours
                                                                </p>
                                                                <a class="text-button text-button--bottom js-analytics" data-g-event="home:content-card" data-g-action="click" data-g-label="Find apps" href="https://play.google.com/store/apps" target="_blank">
                                Find apps&nbsp;
                                <span class="icon icon--right icon--green-arrow-right"></span>
                              </a>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slideshow-controller__container">
                                <span class="slides-pagination-item slideshow-controller__prev js-slideshow-tab" data-tab-id="-1">
              <span class="icon icon--green-arrow-left"></span>
                                </span>
                                <div class="slides-pagination-item">
                                    <div class="slides-pagination-link js-gallery-trigger">
                                        Slide
                                    </div>
                                </div>
                                <div class="slides-pagination-item">
                                    <div class="slides-pagination-link js-gallery-trigger">
                                        Slide
                                    </div>
                                </div>
                                <span class="slides-pagination-item slideshow-controller__next js-slideshow-tab" data-tab-id="1">
              <span class="icon icon--green-arrow-right"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="updates__animation"></div>
                </div>
            </div>

<?php include('footer.php');?>
    <script src="https://www.gstatic.com/external_hosted/gsap/TweenMax.min.js"></script>
    <script src="https://www.gstatic.com/external_hosted/hammerjs/v2_0_2/hammer.min.js"></script>
    <script src="https://www.google.com/jsapi"></script>
    <script src="static/js/main.min.js"></script>
    <script>
        android.init('home');
    </script>
</body>

</html>
