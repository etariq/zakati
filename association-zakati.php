     <?php include('header.php'); ?>
     
	 
	 
	<div class="mdl-card__supporting-text">
        
        <h2 class="tetr text--center mobile-one-whole">  A PROPOS </h2>
        <p class="paragraph mobile-one-whole text--center">
               Le réseau Zakati est ouvert à tous les citoyens du monde, toutes religions confondues, et en harmonie avec les convictions de chacun, formant un peuple commun disposant de droits et devoirs communs, au-delà des clivages nationaux, des barrières socio-culturelles, des considérations ethnico-religieuses, et plaçant l'intérêt de cet ensemble humain au-dessus des intérêts nationaux, économiques et politiques, la communauté Zakati  agit dans le sens d’une citoyenneté mondiale responsable.             
        </p>
	    <p class="paragraph mobile-one-whole text--center">
               Zakati démocratise l’accès a l’emploi, le financement participatif et l’appel a la generosite du public via des campagnes servant a la promotion d’une cause. D’une cause importqnte pour vous.             
        </p>
		<p class="paragraph mobile-one-whole text--center">
               ,et celà, selon un modèle économique reposant sur le financement participatif, et le paiement direct de produits et de services auprès d'établissements partenaires, sans commissions et sans frais.</br>
         Ex : (hôpitaux, pharmacies, écoles, épiceries, stations services, assurances...)
        </p>
	    <p class="paragraph mobile-one-whole text--center">
             Zakati développera des applications open source et des services en ligne afin de proposer une aide a l’emploi et des services financiers alternatifs bases sur la finance éthique participative.          
        </p>
		<h3 class="sous-tetr text--center mobile-one-whole">Définition du réseau social Zakati :</h3>
	    <p class="paragraph mobile-one-whole text--center">
            Comme application de réseau social, Zakati  permet à ses utilisateurs d'entrer des informations personnelles et d'interagir avec d'autres utilisateurs. </br>
Les informations susceptibles d'être mises à la disposition du réseau concernent le type de page Fan ( donateur ou bénéficiaire), puis en sous-categorie définissant ( Individus, Organisations a but non lucratif, organisation a but lucratif, ou gouvernement ), cette base d’informations intégrées dans une case ‘A propos’ avec les études et les compétences ainsi que les centres d'intérêt, les motivations humanitaires, humanistes ou religieuses, les informations bancaires, ainsi que les types de dons souhaitant en faire bénéficier des personnes dans le besoin, associations ou commerces partenaires, ou dans le cas d’un nécessiteux, le genre de besoin recherché. </br>
Ces informations permettent de retrouver des utilisateurs partageant les mêmes intérêts interchangeables, et d’en faire des amis sur le reseau. Ces derniers peuvent former des groupes et y inviter d'autres personnes, et créer un ‘’ Appel à l’action’’.</br>
</p>
<p class="paragraph mobile-one-whole text--center">
Les interactions entre membres incluent le partage de correspondance et de documents multimédias, de créer des campagnes & évènements, servant a la collecte de fonds, ou a la promotion d’une cause commune a un nombre d’utilisateurs amis ou pas sur zakati.</br>
</p>
<h3 class="sous-tetr text--center mobile-one-whole">Principe Zakati :</h3>
<p class="paragraph mobile-one-whole text--center">
Zakati est un outil simple d’utilisation et efficace proposant à ses utilisateurs des fonctionnalités optionnelles appelées « applications », représentées par de petites boîtes superposées sur plusieurs colonnes qui apparaissent à l’affichage de la page de profil de l'utilisateur. Ces applications modifient la page de l'utilisateur et lui permettent de présenter ou échanger des informations aux personnes qui visiteraient sa page. L'utilisateur trouvera par exemple : une liste d'amis, une liste des amis qu'il a en commun avec d’autres amis, une liste de causes qu’il appuie, une liste des campagnes auxquelles il a participé ou qui sont susceptibles de l'intéresser,  les réseaux auxquels l'utilisateur et ses amis appartiennent, une liste des groupes auxquels l'utilisateur appartient, une boîte pour accéder aux photos associées au compte de l'utilisateur, un « mini-feed » résumant les derniers évènements concernant l'utilisateur ou ses amis, sur Zakati et un mur  permettant aux amis de l'utilisateur de laisser de petits messages auxquels l'utilisateur peut répondre.</br>
</p>
<p class="paragraph mobile-one-whole text--center">
Par ailleurs une fonction de messagerie instantanée, permet de signaler à ses amis sa présence en ligne et, si nécessaire, de discuter dans un « salon » privé .</br>

Le chat sur zakati  reste cependant réservé aux membres de Zakati.</br>



Zakati est une association à but non lucratif financée par Dream Capital, le fonds d’investissement participatif de Patrick Chassany.</br>

Le réseau social Zakati est hébergé sur Internet grâce au financement du fonds d'investissement  Dream Capital dépositaire de la marque Zakati. Tout membre peut se créer un profil en tant que personne physique ou une page en tant que personne morale. Zakati est sous licence CC-BY-SA 3.0. </br>	
<a href= https://creativecommons.org/licenses/by-sa/3.0/fr/legalcode  > https://creativecommons.org/licenses/by-sa/3.0/fr/legalcode  </a></br>
</p>
<h3 class="sous-tetr text--center mobile-one-whole">Les degrés de la charité :</h3>
<p class="paragraph mobile-one-whole text--center">
Les 8 degrés de la charité selon la proximité entre le donateur et celui qui reçoit le don, que le don soit connu publiquement ou anonyme, qu'il soit fait de façon spontanée ou sollicitée, qu'il réponde partiellement aux besoins du pauvre ou qu'il lui permette l'auto-subsistance :</br>

La charité préventive : donner du travail à une personne pauvre (ou lui avancer les fonds pour démarrer une affaire) de façon à ce qu'il ne dépende pas de la charité, étant donné qu'on est soi-même indépendant d'elle. D’ou le célèbre principe  : « Donne un poisson à un homme, il mangera un jour. Apprends-lui à pêcher, il mangera toute sa vie ».
Ce niveau est divisé en quatre sous-degrés :</br>
1.	Donner du travail à une personne pauvre. </br>
&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;.	Établir un partenariat avec lui (ceci est considéré comme inférieur, car le bénéficiaire de cette charité pourrait avoir l'impression qu'il n'y participe pas assez). </br>
&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;.	Faire un prêt. </br>
&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;.	Faire un don. </br>
2.	Faire un don anonymement à un récipiendaire inconnu. </br>
3.	Faire un don anonymement à un récipiendaire connu. </br>
4.	Faire un don publiquement à un récipiendaire inconnu. </br>
5.	Faire un don avant qu'on ne la demande. </br>
6.	Faire un don de façon adéquate après qu'on l'a demandée. </br>
7.	Donner de son plein gré, mais inadéquatement (trop peu). </br>
8.	Donner contre son gré. </br>

        </p>
    </div>
	 
	 
	 
	 
	 
	 <?php include('footer.php');?>
    <script src="https://www.gstatic.com/external_hosted/gsap/TweenMax.min.js"></script>
    <script src="https://www.gstatic.com/external_hosted/hammerjs/v2_0_2/hammer.min.js"></script>
    <script src="https://www.google.com/jsapi"></script>
    <script src="static/js/main.min.js"></script>
    <script>
        android.init('home');
    </script>
</body>

</html>
