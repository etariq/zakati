<!DOCTYPE html>
<html class="no-js" lang="en_us" data-geo-code="us">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,target-densitydpi=160" name="viewport">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">

    <title>Zakati.ma</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:700,500,400,300,100&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,200,100&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet">
    <link href="static/img/icon.png" rel="shortcut icon">
    <link href="static/img/touch-icon-iphone.png" rel="apple-touch-icon">
    <link href="static/img/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76">
    <link href="static/img/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120">
    <link href="static/img/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152">
    <link href="static/css/main.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="https://www.gstatic.com/external_hosted/modernizr/modernizr.js"></script>
</head>
<style>

</style>
<body class="index lang-en">
    <div class="header js-search-close js-header" role="banner">
        <div class="navbar js-main-navigation" role="navigation">
            <div class="navbar-header">
                <button class="icon-fade navbar-toggle basement__toggle mobile-one-sixth with-icon js-basement-toggle" data-target-basement="mobile">
                    <div class="wrapper">
                        <span class="icon icon--grey-hamburger-hover"><!-- --></span>
                        <span class="icon icon-fade icon--grey-hamburger"><!-- --></span>
                    </div>
                </button>
                <a class="navbar-brand search-will-hide" href="index.php">
                    <img alt="android" class="logo-android"  src="static/img/logo.png" >
                    <img alt="android" class="logo-android-white" src="static/img/logo.png" >
                    <img alt="android" class="logo-android-one" src="static/img/logo.png">
                </a>
            </div>
            <ol class="nav navbar-nav search-will-hide">
                <li class="dropdown--extended dropdown--discover js-dropdown js-search--fadable">
                    <a class="menu-button dropdown-toggle js-dropdown-toggle js-analytics" data-g-action="click" data-g-event="top-navigation" data-g-label="dropdown-toggle-discover">
            Découvrir
            <span class="icon icon-caret"><!-- --></span></a>
                    <ul class="dropdown-menu card" role="menu">
                        <li class="nav-item nav__item--story">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-about" data-g-label="android-story" href="resaux-zakati.php">
                                <span class="icon sprite-icon-story"><img src="img/module_smscenter.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Le réseau Zakati</a>
                        </li>
                        <li class="nav-item nav__item--story">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-about" data-g-label="android-story" href="trouver-emploi.php">
                                <span class="icon sprite-icon-story"><img src="img/recherche-emploi.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Trouver un emploi</a>
                        </li>
                        <li class="nav-item nav__item--marshmallow">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-about"  href="financer-projet.php">
                                <span class="icon sprite-icon-marshmallow"><img src="img/icone.png" style="width: 150px; height: 135px;"/><!-- --></span> Financer un projet</a>
                        </li>
                        <li class="nav-item nav__item--switch">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-about"  href="faire-don.php">
                                <span class="icon sprite-icon-switch"><img src="img/don.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Faire un don</a>
                        </li>
                        <li class="nav-item nav__item--work">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-about"  href="payer-zakati.php">
                                <span class="icon sprite-icon-work"><img src="img/images.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Payer sa Zakat</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown--extended dropdown--devices js-dropdown js-search--fadable">
                    <a class="menu-button dropdown-toggle js-dropdown-toggle js-analytics" data-g-action="click" data-g-event="top-navigation" data-g-label="dropdown-toggle-devices">
            Contribuer
            <span class="dropdown-indicator icon icon-caret"><!-- --></span></a>
                    <ul class="dropdown-menu card" role="menu">
                        <li class="nav-item nav__item--phones">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-devices" data-g-label="phones" href="collaborer.php">
                                <span class="icon sprite-icon-phones"><img src="img/recherche-emploi.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Collaborer </a>
                        </li>
                        <li class="nav-item nav__item--tablets">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-devices" data-g-label="tablets" href="collecter.php">
                                <span class="icon sprite-icon-tablets"><img src="img/recherche-emploi.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Collecter</a>
                        </li>
                        <li class="nav-item nav__item--tv">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-devices" data-g-label="tv" href="redistribuer.php">
                                <span class="icon sprite-icon-tv"><img src="img/recherche-emploi.jpg" style="width: 150px; height: 135px;"/><!-- --></span> Redistribuer</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav__item--play js-search--fadable">
                    <a class="js-analytics" data-g-action="click" data-g-event="top-navigation" data-g-label="play" href="campagnes.php"> Campagnes</a>
                </li>
                <li class="nav-item nav__item--promo nav__item--pay js-search--fadable">
                    <a class="js-analytics" data-g-action="click" data-g-event="top-navigation" data-g-label="promo" href="connecter.php">Se Connecter</a>
                </li>
                <li class="dropdown dropdown--overflow js-dropdown js-search--fadable">
                    <a class="menu-button dropdown-toggle js-dropdown-toggle js-analytics" data-g-action="click" data-g-event="top-navigation" data-g-label="dropdown-toggle-overflow">
            A propos 
            <span class="dropdown-indicator icon icon-caret"><!-- --></span></a>
                    <ul class="dropdown-menu card" role="menu">
                        <li class="nav-item nav__item--one">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-overflow" data-g-label="android-one" href="association-zakati.php"> L'association Zakati</a>
                        </li>
                        <li class="nav-item nav__item--preview">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-overflow" data-g-label="offsite:m-developer-preview" href="nous-contacter.php"> Nous contacter</a>
                        </li>
                        <li class="nav-item nav__item--resources">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-overflow" data-g-label="offsite:developer-resources" href="aide-faq.php"> Aide / FAQ</a>
                        </li>
                        <li class="nav-item nav__item--resources">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-overflow" data-g-label="offsite:developer-resources" href="http://blog.zakati.ma/" target="_blank"> Blog</a>
                        </li>
                        <li class="nav-item nav__item--resources">
                            <a class="js-analytics" data-g-action="click" data-g-event="top-navigation:dropdown-overflow" data-g-label="offsite:developer-resources" href="http://wiki.zakati.ma" target="_blank"> Wiki</a>
                        </li>
                    </ul>
                </li>
            </ol>
            <div class="nav-search">
                <form action="https://www.android.com/results/" method="get" class="animated-search-box js-animated-search-box js-analytics" data-g-event="top-navigation" data-g-action="click" data-g-label="search">
                    <div class="animated-search-box__inner js-animated-search-box__inner">
                        <label class="animated-search-box__label" for="q">Search</label>
                        <input type="text" class="animated-search-box__input js-animated-search-box__input" id="q" name="q" value="" placeholder="Search" />
                        <div class="animated-search-box__close js-animated-search-box__close"></div>
                        <div class="animated-search-box__handle">
                            <div class="animated-search-box__handle-inner"></div>
                        </div>
                        <div class="animated-search-box__pad"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="page page--wrapper js-basement-wrapper" id="top">
        <div class="page-content js-basement-under">

            
                