 <div class="footer js-search-close" id="bottom" role="contentinfo">
                <ul class="list-unstyled footer-social row">
                    <li class="nav-item">
                        <a class="icon-fade js-analytics" data-g-action="click" data-g-event="footer:social" data-g-label="offsite:google-plus" href="#" target="_blank">
                            <div class="wrapper">
                                <div class="icon icon--footer-google-plus-red">
                                    <!-- -->
                                </div>
                                <div class="icon icon--footer-google-plus-grey">
                                    <!-- -->
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="icon-fade js-analytics" data-g-action="click" data-g-event="footer:social" data-g-label="offsite:twitter" href="#" target="_blank">
                            <div class="wrapper">
                                <div class="icon icon--footer-twitter-blue">
                                    <!-- -->
                                </div>
                                <div class="icon icon--footer-twitter-grey">
                                    <!-- -->
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="icon-fade js-analytics" data-g-action="click" data-g-event="footer:social" data-g-label="offsite:facebook" href="https://www.facebook.com/Zakati-1017718951593564/?ref=ts&fref=ts" target="_blank">
                            <div class="wrapper">
                                <div class="icon icon--footer-facebook-blue">
                                    <!-- -->
                                </div>
                                <div class="icon icon--footer-facebook-grey">
                                    <!-- -->
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="icon-fade js-analytics" data-g-action="click" data-g-event="footer:social" data-g-label="#" target="_blank">
                            <div class="wrapper">
                                <div class="icon icon--footer-youtube-red">
                                    <!-- -->
                                </div>
                                <div class="icon icon--footer-youtube-grey">
                                    <!-- -->
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="icon-fade js-analytics" data-g-action="click" data-g-event="footer:social" data-g-label="offsite:blogger" href="#" target="_blank">
                            <div class="wrapper">
                                <div class="icon icon--footer-blogger-orange">
                                    <!-- -->
                                </div>
                                <div class="icon icon--footer-blogger-grey">
                                    <!-- -->
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
                


                <ul class="list-unstyled footer-info row">
                    <li>
                        <a class="google-link js-analytics" data-g-action="click" data-g-event="footer" data-g-label="offsite:google-dot-com" href="http://zakati.ma/" target="_blank">
                            <span class="icon icon--footer-google-grey"><!-- --></span>
                        </a>
                    </li>
                    <li class="nav-item dropdown js-dropdown">
                        <a class="dropdown-toggle js-dropdown-toggle js-analytics" data-g-action="click" data-g-event="footer" data-g-label="dropdown-toggle:versions">
        Versions
        <span class="dropdown-indicator icon icon-caret"><!-- --></span></a>
                        <ul class="dropdown-menu card" role="menu">
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:versions" data-g-label="lollipop-5-0" href="versions/lollipop-5-0/index.html">5.0 Lollipop</a>
                            </li>
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:versions" data-g-label="kit-kat-4-4" href="versions/kit-kat-4-4/index.html">4.4 KitKat</a>
                            </li>
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:versions" data-g-label="jelly-bean-4-3" href="versions/jelly-bean-4-3/index.html">4.3 Jelly Bean</a>
                            </li>
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:versions" data-g-label="android-history" href="history/index.html">Android History</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown js-dropdown">
                        <a class="dropdown-toggle js-dropdown-toggle js-analytics" data-g-action="click" data-g-event="footer" data-g-label="dropdown-toggle:developers">
        For developers
        <span class="dropdown-indicator icon icon-caret"><!-- --></span></a>
                        <ul class="dropdown-menu card" role="menu">
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:developers" data-g-label="developer-resources" href="https://developer.android.com/">App Developer Resources</a>
                            </li>
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:developers" data-g-label="android-open-source" href="http://source.android.com/">Android Open Source Project</a>
                            </li>
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="footer:dropdown:developers" data-g-label="android-sdk" href="https://developer.android.com/sdk/">Android SDK</a>
                            </li>
                            <li>
                                <a class="js-analytics" data-g-action="click" data-g-event="basement:developers" data-g-label="offsite:android-for-work" href="http://developer.android.com/work">Android for Work</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="js-analytics" data-g-action="click" data-g-event="footer" data-g-label="blog" href="http://blog.zakati.ma/" target="_blank">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="js-analytics" data-g-action="click" data-g-event="footer" data-g-label="offsite:privacy-policy" href="http://wiki.zakati.ma" target="_blank">Wiki</a>
                    </li>
                    <li class="language-select">
                        <label>
                           
                        </label>
                    </li>
                </ul>
                <a href="index.html#" class="back-to-top icon-fade js-scroll" data-target="top">
                    <span class="icon-fade-text"></span>
                    <span class="wrapper">
      <span class="icon icon--footer-arrow-up-green"><!-- --></span>
                    <span class="icon icon--footer-arrow-up-grey"><!-- --></span>
                    </span>
                </a>
            </div>
        </div>

        <div class="basement nav--active-page-labels js-basement js-basement-close js-basement--mobile js-basement-over--mobile" data-basement-name="mobile" data-push-content="false" data-slide-direction="right">
            <div class="basement-nav js-nav-width">
                <div class="basement-header" style="background-color:#fff;">
                    <a class="logo js-analytics" data-g-action="click" data-g-event="basement" data-g-label="home" href="index.html">
                        <img  class="logo-android-white" height="64" src="static/img/logo.png" >
                    </a>
                </div>
                <ul class="nav nav-pages" role="navigation">
                    <li class="nav-item nav__item--story">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement" data-g-label="android-story" href="resaux-zakati.php">Le reseau Zakati</a>
                    </li>
                    <li class="divider">
                        <!-- -->
                    </li>
                    <li class="nav-item nav__item--marshmallow">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="trouver-emploi.php">&nbsp;&nbsp;Trouver un emploi</a>
                    </li>
                    <li class="nav-item nav__item--marshmallow">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="financer-projet.php">&nbsp;&nbsp;Financer un projet </a>
                    </li>
                    <li class="nav-item nav__item--marshmallow">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="faire-don.php">&nbsp;&nbsp;Faire Un Don</a>
                    </li>
                    <li class="nav-item nav__item--marshmallow">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="payer-zakati.php">&nbsp;&nbsp;Payer sa Zakat</a>
                    </li>
                    <li class="divider">
                        <!-- -->
                    </li>
                    <li class="nav-item nav__item--switch">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="campagnes">Campagnes</a>
                    </li>
                    <li class="nav-item nav__item--switch">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="collabore.php">&nbsp;&nbsp;Collaborer</a>
                    </li>
                    <li class="nav-item nav__item--switch">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="collecter.php">&nbsp;&nbsp;Collecter</a>
                    </li>
                    <li class="nav-item nav__item--switch">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="redistribuer.php">&nbsp;&nbsp;Redistribuer</a>
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="connecter.php">Se connecter</a>
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="#">Apropos</a>
                    </li>
                    <li class="divider">
                        <!-- -->
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="association-zakati.php">L'association Zakti</a>
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="nous-contacter.php">Nous Contacter</a>
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="aide-faq.php">Aide / FAQ</a>
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="http://blog.zakati.ma/" target="_blank">Blog</a>
                    </li>
                    <li class="nav-item nav__item--work">
                        <a class="js-analytics" data-g-action="click" data-g-event="basement"  href="http://wiki.zakati.ma" target="_blank">Wiki</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>